The dataset includes the following 13 variables:

age = the age of the individual in years

workclass = the classification of the individual's working status (does the person work 
            for the federal government, work for the local government, work without pay, 
            and so on)

education = the level of education of the individual (e.g., 5th-6th grade, high school
             graduate, PhD, so on)
             
maritalstatus = the marital status of the individual

occupation = the type of work the individual does (e.g., administrative/clerical work, 
            farming/fishing, sales and so on)
relationship = relationship of individual to his/her household

race = the individual's race

sex = the individual's sex

capitalgain = the capital gains of the individual in 1994 (from selling an asset such 
            as a stock or bond for more than the original purchase price)
            
capitalloss = the capital losses of the individual in 1994 (from selling an asset such 
               as a stock or bond for less than the original purchase price)
               
hoursperweek = the number of hours the individual works per week

nativecountry = the native country of the individual

over50k = whether or not the individual earned more than $50,000 in 1994